import React from 'react'
import Carditem from './Carditem'
import './Cards.css'

function Cards() {
    return (
        <div className='cards'>
            <h1>Available Products</h1>
        <div className='cards__container'>
            <div className='cards__wrapper'>
                    <ul className='cards_items'>
                    <Carditem />
                    </ul>
                </div>
            </div>  
        </div>
    )
}

export default Cards
